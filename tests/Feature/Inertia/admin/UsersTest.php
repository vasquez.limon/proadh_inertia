<?php

namespace Tests\Feature\Inertia\users;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UsersTest extends TestCase
{
    use RefreshDatabase;

    protected $user = [
        'name' => 'Faustino Vasquez',
        'email' => 'fvasquez@local.com',
        'password' => 'password',
        'password_confirmation' => 'password'
    ];


    /** @test **/
    public function admin_user_can_create_users()
    {

        $this->actingAs($admin = User::factory()->withPersonalTeam()->create())
            ->post(route('admin.user.store'), $this->user)
            ->assertRedirect(route('admin.user.index'));

        $this->assertDatabaseCount('users', 2);
    }


    /** @test **/
    public function admin_user_can_update_users()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($admin = User::factory()->withPersonalTeam()->create())
            ->put(route('admin.user.update', $user), [
                'name' => 'Sebastian Vasquez',
                'email' => $user->email,
            ])
            ->assertRedirect(route('admin.user.index'));


        $this->assertDatabaseHas('users', [
            'name' => 'Sebastian Vasquez'
        ]);
    }


    /** @test **/
    public function admin_user_can_delete_users()
    {
        $user = User::factory()->create();

        $response = $this->actingAs($admin = User::factory()->withPersonalTeam()->create())
            ->delete(route('admin.user.destroy', $user))
            ->assertRedirect(route('admin.user.index'));

        $this->assertDatabaseCount('users', 1);
    }
}
