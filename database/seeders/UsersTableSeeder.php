<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::factory()->withPersonalTeam()->create([
            'name' => 'Faustino Vasquez',
            'email' => 'fvasquez@local.com'
        ]);

        User::factory()->withPersonalTeam()->times(20)->create();
    }
}
